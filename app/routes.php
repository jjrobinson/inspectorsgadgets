<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*
Route::get('/', function()
{
	return View::make('login');
});
 * 
 */

//Route::get('/', 'HomeController@showWelcome');

//Data Access
Route::get('/viewDB', 'ViewController@showDB');
Route::get('/', 'ViewController@showDB');

//Authentication
Route::get('login', array('as' => 'login', 'uses' => 'AuthController@showLogin'));
Route::post('login', 'AuthController@postLogin');
Route::get('logout', 'AuthController@getLogout');

// Secure-Routes
Route::group(array('before' => 'login'), function()
{
    Route::get('secret', 'HomeController@showSecret');
});